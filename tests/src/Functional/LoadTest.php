<?php

namespace Drupal\Tests\aadhaar_number_widget\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Aadhaar Number Widget Module Load Test.
 *
 * @group aadhaar_number_widget
 */
class LoadTest extends BrowserTestBase {

  /**
   * Summary of defaultTheme.
   *
   * @var mixed
   */
  protected $defaultTheme = 'stark';

  /**
   * Summary of modules.
   *
   * @var mixed
   */
  protected static $modules = [
    'node',
    'block',
    'field_ui',
    'aadhaar_number_widget',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage after enabling Aadhaar Number Widget Module.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Tests the Aadhaar Number Widget module unistall.
   */
  public function testModuleUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Aadhaar Number Widget');
    $this->submitForm(['uninstall[aadhaar_number_widget]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Aadhaar Number Widget');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests Aadhaar Number Widget module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the Aadhaar Number Widget module.
    $this->container->get('module_installer')->uninstall(['aadhaar_number_widget'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[aadhaar_number_widget][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install Aadhaar Number Widget');
    $assert_session->pageTextContains('Module Aadhaar Number Widget has been enabled');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
