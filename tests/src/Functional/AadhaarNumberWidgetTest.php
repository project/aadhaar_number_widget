<?php

namespace Drupal\Tests\aadhaar_number_widget\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Class to Test Aadhaar Number Widget.
 *
 * @group aadhaar_number_widget
 */
class AadhaarNumberWidgetTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Summary of defaultTheme.
   *
   * @var mixed
   */
  protected $defaultTheme = 'stark';

  /**
   * Summary of modules.
   *
   * @var mixed
   */
  protected static $modules = [
    'node',
    'field_ui',
    'block',
    'aadhaar_number_widget',
  ];

  /**
   * The bundle being tested.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The content type being tested.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $contentType;

  /**
   * A user that can edit content types.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    // Create a content type.
    $this->bundle = $this->randomMachineName();
    $this->contentType = $this->drupalCreateContentType(['type' => $this->bundle]);

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer node fields',
      'administer node display',
      'administer node form display',
      "create $this->bundle content",
      "edit any $this->bundle content",
      "edit own $this->bundle content",
      "access content",
    ]);
    $this->drupalLogin($this->adminUser);
    // Breadcrumb is required for FieldUiTestTrait::fieldUIAddNewField.
    $this->drupalPlaceBlock('system_breadcrumb_block');

  }

  /**
   * Tests the Aadhaar Number Widget for field.
   */
  public function testWidget() {

    $type_path = 'admin/structure/types/manage/' . $this->bundle;

    // Add a link field to the newly-created type.
    $label = $this->randomMachineName();
    $field_name = mb_strtolower($label);
    $storage_settings = ['cardinality' => 'number', 'cardinality_number' => 1];
    $this->fieldUIAddNewField($type_path, $field_name, $label, 'string', $storage_settings);
    $this->assertSession()->pageTextContains('Saved ' . $field_name . ' configuration');

    // Manually clear cache on the tester side.
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();

    $bundle_path = 'admin/structure/types/manage/' . $this->bundle;
    // Check if field exists.
    $this->drupalGet($bundle_path . '/fields');
    $this->assertSession()->pageTextContains($label);

    // Check that the field appears in the overview form.
    $xpath = $this->assertSession()->buildXPathQuery("//table[@id=\"field-overview\"]//tr/td[1 and text() = :label]", [
      ':label' => $label,
    ]);
    $this->assertSession()->elementExists('xpath', $xpath);

    $this->drupalGet($bundle_path . '/form-display');
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'fields[field_' . $field_name . '][type]' => 'aadhaar_number_widget',
    ];

    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->assertSession()->pageTextContains('Aadhaar Number Widget');

    // Add Content.
    $this->drupalGet('node/add/' . $this->bundle);
    $this->assertSession()->pageTextContains($label);

    // Create a node.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName(8);
    $edit['body[0][value]'] = $this->randomMachineName(16);
    $edit['field_' . $field_name . '[0][value]'] = '12345';
    $this->submitForm($edit, 'Save');
    // Check that the error message is displayed
    // when using invalid Aadhaar number.
    $this->assertSession()->pageTextContains('Aadhaar number is not valid.');
    $this->assertSession()->pageTextNotContains($this->bundle . ' ' . $edit['title[0][value]'] . ' has been created.');

    // @see https://www.uidai.gov.in/en/916-developer-section/data-and-downloads-section/11350-testing-data-and-license-keys.html for test Aadhaar data used in tests.
    $edit1 = [];
    $edit1['title[0][value]'] = $this->randomMachineName(8);
    $edit1['body[0][value]'] = $this->randomMachineName(16);
    $edit1['field_' . $field_name . '[0][value]'] = '999941057057';
    $this->submitForm($edit1, 'Save');
    // Check that the error message is displayed
    // when using invalid Aadhaar number with 12 digits.
    $this->assertSession()->pageTextContains('Aadhaar number is not valid.');
    $this->assertSession()->pageTextNotContains($this->bundle . ' ' . $edit1['title[0][value]'] . ' has been created.');

    $edit2 = [];
    $edit2['title[0][value]'] = $this->randomMachineName(8);
    $edit2['body[0][value]'] = $this->randomMachineName(16);
    $edit2['field_' . $field_name . '[0][value]'] = '999941057058';
    $this->submitForm($edit2, 'Save');

    // Check that the Node has been created without errors.
    $this->assertSession()->pageTextNotContains('Aadhaar number is not valid.');
    $this->assertSession()->pageTextContains($this->bundle . ' ' . $edit2['title[0][value]'] . ' has been created.');

    // Check that the node exists in the database.
    $created_node = $this->drupalGetNodeByTitle($edit2['title[0][value]']);
    $this->assertNotEmpty($created_node, 'Node found in database.');

    // Check that the newly created node page is accessible.
    $this->drupalGet('node/' . $created_node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Edit the created Node.
    $this->drupalGet('node/' . $created_node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $edit3 = [];
    $edit3['field_' . $field_name . '[0][value]'] = '999941057059';
    $this->submitForm($edit3, 'Save');

    // Check that the Node is not updated with invalid Aadhaar Number.
    $this->assertSession()->pageTextContains('Aadhaar number is not valid.');
    $this->assertSession()->pageTextNotContains($this->bundle . ' ' . $edit2['title[0][value]'] . ' has been updated.');

    $edit4 = [];
    $edit4['field_' . $field_name . '[0][value]'] = '999971658847';
    $this->submitForm($edit4, 'Save');
    // Check that the Node has been updated without errors.
    $this->assertSession()->pageTextNotContains('Aadhaar number is not valid.');
    $this->assertSession()->pageTextContains($this->bundle . ' ' . $edit2['title[0][value]'] . ' has been updated.');

    // Check that the updated node page is accessible.
    $this->drupalGet('node/' . $created_node->id());
    $this->assertSession()->statusCodeEquals(200);

  }

}
