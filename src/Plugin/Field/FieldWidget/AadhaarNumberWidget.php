<?php

namespace Drupal\aadhaar_number_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'aadhaar_number_widget' widget.
 *
 * @FieldWidget(
 *   id = "aadhaar_number_widget",
 *   module = "aadhaar_number_widget",
 *   label = @Translation("Aadhaar Number Widget"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class AadhaarNumberWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#maxlength' => 14,
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];

    return $element;
  }

  /**
   * Validate the color text field.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];

    // Return if value is empty.
    if (strlen($value) == 0) {
      return;
    }

    // Aadhar number input patten.
    // Group #1 968415522551
    // Group #2 9684 1552 2551
    // Group #3 9684-1552-2551.
    $pattern = "/(^[0-9]{4}[0-9]{4}[0-9]{4}$)|(^[0-9]{4}\s[0-9]{4}\s[0-9]{4}$)|(^[0-9]{4}-[0-9]{4}-[0-9]{4}$)/";

    if (preg_match($pattern, $value)) {
      // Aadhar number check algorithm.
      if (self::isAadharValid($value) == 0) {
        $form_state->setError($element, $this->t("Aadhaar number is not valid."));
      }
    }
    else {
      $form_state->setError($element, $this->t("The Aadhaar number is not valid. you can follow the bellow pattern. <br> Group #1 968415522551 <br> Group #2 9684 1552 2551 <br> Group #3 9684-1552-2551"));
    }

  }

  /**
   * Check aadhar number is valid.
   */
  public static function isAadharValid($num) {
    $num = preg_replace('/[-]|\s+/', '', $num);
    settype($num, "string");
    $expectedDigit = substr($num, -1);
    $actualDigit = self::checkAadhaarDigit(substr($num, 0, -1));

    return ($expectedDigit == $actualDigit) ?? 0;
  }

  /**
   * Aadhar digit check algorithm.
   */
  protected static function checkAadhaarDigit($partial) {
    $dihedral = [
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
      [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
      [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
      [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
      [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
      [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
      [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
      [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
      [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
    ];
    $permutation = [
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
      [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
      [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
      [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
      [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
      [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
      [7, 0, 4, 6, 9, 1, 3, 2, 5, 8],
    ];
    $inverse = [0, 4, 3, 2, 1, 5, 6, 7, 8, 9];
    settype($partial, "string");
    $partial = strrev($partial);
    $digitIndex = 0;
    for ($i = 0; $i < strlen($partial); $i++) {
      $digitIndex = $dihedral[$digitIndex][$permutation[($i + 1) % 8][$partial[$i]]] ?? NULL;
    }
    return $inverse[$digitIndex];
  }

}
