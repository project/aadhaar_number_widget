# Aadhaar Number Widget

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Aadhaar Card is a unique identity card issued to the Indian citizen by the
Unique "Identification Authority of India- UIDAI". The card has an
arbitrary grouping of 12 numerical figures that are unique for each
Aadhaar cardholder.This 12-digit number is known as the Aadhaar card
number and is used to identify individuals. The card comprises the
demographic and biometric data of the person and therefore is used
mainly for authentication.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- `composer require drupal/aadhaar_number_widget`
- `drush en aadhaar_number_widget -y`


## Configuration

Allow the text field widget aadhaar number validate formatter.


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
